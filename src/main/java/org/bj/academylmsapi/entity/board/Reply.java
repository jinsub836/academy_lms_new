package org.bj.academylmsapi.entity.board;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.board.reply.ReplyRequest;

import java.time.LocalDateTime;

// 게시판 - 답변 entity
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Reply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 게시판 id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "boardId")
    private Board board;

    // 회원등급
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MemberType memberType;

    // 멤버 id
    @Column(nullable = false)
    private Long memberId;

    // 답변 제목
    @Column(nullable = false, length = 30)
    private String boardReTitle;

    // 답변 작성자
    @Column(nullable = false)
    private String username;

    // 내용
    @Column(nullable = false, columnDefinition = "TEXT")
    private String boardContent;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putReply(ReplyRequest request) {
        this.boardReTitle = request.getBoardReTitle();
        this.boardContent = request.getBoardContent();
    }

    private Reply(Builder builder) {
        this.board = builder.board;
        this.memberType = builder.memberType;
        this.memberId = builder.memberId;
        this.boardReTitle = builder.boardReTitle;
        this.username = builder.username;
        this.boardContent = builder.boardContent;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<Reply> {
        private final Board board;
        private final MemberType memberType;
        private final Long memberId;
        private final String boardReTitle;
        private final String username;
        private final String boardContent;
        private final LocalDateTime dateCreate;

        public Builder(ReplyRequest request, Board board) {
            this.board = board;
            this.memberType = request.getMemberType();
            this.memberId = request.getMemberId();
            this.boardReTitle ="[RE]"+board.getBoardTitle();
            this.username = request.getUsername();
            this.boardContent = request.getBoardContent();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Reply build() {
            return new Reply(this);
        }
    }
}
