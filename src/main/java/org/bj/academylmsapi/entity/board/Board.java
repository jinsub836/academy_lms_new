package org.bj.academylmsapi.entity.board;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.board.board.BoardRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 게시판 - 게시판 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원등급
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MemberType memberType;

    // 멤버 id (수강생이든 강사든..)
    @Column(nullable = false)
    private Long memberId;

    // 제목
    @Column(nullable = false, length = 30)
    private String boardTitle;

    // 작성자
    @Column(nullable = false)
    private String username;

    // 내용
    @Column(nullable = false, columnDefinition = "TEXT")
    private String boardContent;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putBoard(BoardRequest request) {
        this.boardTitle = request.getBoardTitle();
        this.boardContent = request.getBoardContent();
    }

    private Board(Builder builder) {
        this.memberType = builder.memberType;
        this.memberId = builder.memberId;
        this.boardTitle = builder.boardTitle;
        this.username = builder.username;
        this.boardContent = builder.boardContent;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<Board> {
        private final MemberType memberType;
        private final Long memberId;
        private final String boardTitle;
        private final String username;
        private final String boardContent;
        private final LocalDateTime dateCreate;

        public Builder(BoardRequest request) {
            this.memberType = request.getMemberType();
            this.memberId = request.getMemberId();
            this.boardTitle = request.getBoardTitle();
            this.username = request.getUsername();
            this.boardContent = request.getBoardContent();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Board build() {
            return new Board(this);
        }
    }
}
