package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.FinalEducation;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.enums.StudentStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.student.StudentInfoChangeAdminRequest;
import org.bj.academylmsapi.model.student.StudentInfoChangeRequest;
import org.bj.academylmsapi.model.student.StudentPasswordChangeRequest;
import org.bj.academylmsapi.model.student.StudentRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

// 수강생 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Student implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 과정명 선택id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "subjectId")
    private Subject subject;

    // 사진주소
    @Column(columnDefinition = "TEXT")
    private String imgSrc;

    // 이름
    @Column(nullable = false, length = 20)
    private String studentName;

    // 수강생 아이디
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    // 수강생 비밀번호
    @Column(nullable = false, columnDefinition = "TEXT")
    private String password;

    // 희원 등급
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberType memberType;

    // 성별
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 본인휴대폰
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    // 주민등록번호
    @Column(nullable = false, length = 14)
    private String idNumber;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 이메일
    @Column(nullable = false, length = 50, unique = true)
    private String email;

    // 최종학력
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private FinalEducation finalEducation;

    // 전공
    @Column(nullable = false, length = 20)
    private String major;

    // 수강상태
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private StudentStatus studentStatus;

    // 상태변경일
    private LocalDate dateStatusChange;

    // 상태변경사유
    @Column(length = 20)
    private String statusWhy;

    // 가입일
    @Column(nullable = false)
    private LocalDate dateJoin;

    public void putStudentPassword(StudentPasswordChangeRequest request){
        this.password = request.getPassword();
    }

    public void putStudentInfoAdmin(Subject subject, StudentInfoChangeAdminRequest request){
        this.subject = subject;
        this.imgSrc = request.getImgSrc();
        this.studentName = request.getStudentName();
        this.username = request.getUsername();
        this.password = request.getPassword();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.phoneNumber = request.getPhoneNumber();
        this.idNumber = request.getIdNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
        this.finalEducation = request.getFinalEducation();
        this.major = request.getMajor();
        this.studentStatus = request.getStudentStatus();
        this.dateStatusChange = request.getDateStatusChange();
        this.statusWhy = request.getStatusWhy();
    }

    public void putStudentInfo(StudentInfoChangeRequest request){
        this.imgSrc = request.getImgSrc();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
        this.finalEducation = request.getFinalEducation();
        this.major =request.getMajor();
        this.username = request.getUsername();
        this.password=request.getPassword();
    }

    private Student(Builder builder){
        this.subject = builder.subject;
        this.imgSrc = builder.imgSrc;
        this.studentName = builder.studentName;
        this.username = builder.username;
        this.password = builder.password;
        this.memberType= builder.memberType;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.idNumber = builder.idNumber;
        this.address = builder.address;
        this.email = builder.email;
        this.finalEducation = builder.finalEducation;
        this.major = builder.major;
        this.studentStatus = builder.studentStatus;
        this.dateStatusChange = builder.dateStatusChange;
        this.statusWhy = builder.statusWhy;
        this.dateJoin = builder.dateJoin;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static class Builder implements CommonModelBuilder<Student>{
        private final Subject subject;
        private final  String imgSrc;
        private final String studentName;
        private final String username;
        private final String password;
        private final MemberType memberType;
        private final  Gender gender;
        private final  LocalDate dateBirth;
        private final  String phoneNumber;
        private final  String idNumber;
        private final  String address;
        private final  String email;
        private final  FinalEducation finalEducation;
        private final  String major;
        private final StudentStatus studentStatus;
        private final LocalDate dateStatusChange;
        private final String statusWhy;
        private final LocalDate dateJoin;

        public Builder(StudentRequest studentRequest , Subject subject){
            this.subject = subject;
            this.imgSrc = studentRequest.getImgSrc();
            this.studentName = studentRequest.getStudentName();
            this.username = studentRequest.getUsername();
            this.password = studentRequest.getPassword();
            this.memberType =MemberType.ROLE_GENERAL;
            this.gender = studentRequest.getGender();
            this.dateBirth = studentRequest.getDateBirth();
            this.phoneNumber = studentRequest.getPhoneNumber();
            this.idNumber = studentRequest.getIdNumber();
            this.address = studentRequest.getAddress();
            this.email = studentRequest.getEmail();
            this.finalEducation = studentRequest.getFinalEducation();
            this.major = studentRequest.getMajor();
            this.studentStatus = StudentStatus.INCLASS;
            this.dateStatusChange = null;
            this.statusWhy = "-";
            this.dateJoin = LocalDate.now();
        }
        @Override
        public Student build() {
            return new Student(this);
        }
    }
}
