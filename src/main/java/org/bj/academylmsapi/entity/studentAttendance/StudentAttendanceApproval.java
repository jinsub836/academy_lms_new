package org.bj.academylmsapi.entity.studentAttendance;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalFixRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// 수강생 출결 승인 entity

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class StudentAttendanceApproval {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생 id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 시작일
    @Column(nullable = false)
    private LocalDate dateStart;

    // 종료일
    @Column(nullable = false)
    private LocalDate dateEnd;

    // 사유
    @Column(nullable = false, length = 30)
    private String reason;

    // 첨부파일 주소
    @Column(columnDefinition = "TEXT")
    private String addFile;

    // 출결유형
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private AttendanceType attendanceType;

    // 승인상태
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private ApprovalState approvalState;

    // 등록일
    @Column(nullable = false)
    private LocalDate dateRegister;

    public void putStudentAttendanceApprovalAdmin(){
        this.approvalState = ApprovalState.ADMINAPPROVAL;
    }

    public void putStudentAttendanceApproval(StudentAttendanceApprovalFixRequest request) {
        this.dateStart = request.getDateStart();
        this.dateEnd = request.getDateEnd();
        this.reason = request.getReason();
        this.addFile = request.getAddFile();
        this.attendanceType = request.getAttendanceType();
        this.approvalState = request.getApprovalState();
    }

    private StudentAttendanceApproval(Builder builder) {
        this.student = builder.student;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.reason = builder.reason;
        this.addFile = builder.addFile;
        this.attendanceType = builder.attendanceType;
        this.approvalState = builder.approvalState;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApproval> {
        private final Student student;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final String reason;
        private final String addFile;
        private final AttendanceType attendanceType;
        private final ApprovalState approvalState;
        private final LocalDate dateRegister;

        public Builder(StudentAttendanceApprovalRequest request, Student student) throws ParseException {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            this.student = student;
            this.dateStart = LocalDate.parse(request.getDateStart(), format);
            this.dateEnd = LocalDate.parse(request.getDateEnd(), format);
            this.reason = request.getReason();
            this.addFile = request.getAddFile();
            this.attendanceType = request.getAttendanceType();
            this.approvalState = ApprovalState.NOTAPPROVED;
            this.dateRegister = LocalDate.now();
        }

        @Override
        public StudentAttendanceApproval build() {
            return new StudentAttendanceApproval(this);
        }
    }

}
