package org.bj.academylmsapi.entity.testManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.TestStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.testManagement.Answer.AnswerPutScoreRequest;
import org.bj.academylmsapi.model.testManagement.Answer.AnswerPutStudentSignRequest;
import org.bj.academylmsapi.model.testManagement.Answer.AnswerTestRequest;

import java.time.LocalDateTime;

// 평가관리 - 시험채점
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AnswerTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 시험출제id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "makeTestId", nullable = false)
    private MakeTest makeTest;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 답변
    @Column(nullable = false, columnDefinition = "TEXT")
    private String testAnswer;

    // 수강생서명
    private String studentSign;

    // 교수자총평
    @Column(columnDefinition = "TEXT")
    private String teacherComment;

    // 평가점수
    @Column(length = 3)
    private Short studentScore;

    // 성취수준
    @Column(length = 1)
    private Short achieveLevel;

    // 응시현황
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private TestStatus testStatus;

    // 첨부파일
    @Column(columnDefinition = "TEXT")
    private String addFile;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void AnswerTestSign(AnswerPutStudentSignRequest request){
        this.studentSign = request.getStudentSign();
        this.testStatus = TestStatus.SIGNOKAY;
    }

    public void getScore(AnswerPutScoreRequest request){
        this.studentScore = request.getStudentScore();
        this.teacherComment = request.getTeacherComment();
        this.achieveLevel = request.getAchieveLevel();
    }


    private AnswerTest(Builder builder) {
        this.makeTest = builder.makeTest;
        this.student = builder.student;
        this.testAnswer = builder.testAnswer;
        this.studentSign = builder.studentSign;
        this.teacherComment = builder.teacherComment;
        this.studentScore = builder.studentScore;
        this.achieveLevel = builder.achieveLevel;
        this.testStatus = builder.testStatus;
        this.addFile = builder.addFile;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<AnswerTest> {
        private final MakeTest makeTest;
        private final Student student;
        private final String testAnswer;
        private final String studentSign;
        private final String teacherComment;
        private final Short studentScore;
        private final Short achieveLevel;
        private final TestStatus testStatus;
        private final String addFile;
        private final LocalDateTime dateRegister;

        public Builder(MakeTest makeTest, Student student, AnswerTestRequest request) {
            this.makeTest = makeTest;
            this.student = student;
            this.testAnswer = request.getTestAnswer();
            this.studentSign = request.getStudentSign();
            this.teacherComment = request.getTeacherComment();
            this.studentScore = request.getStudentScore();
            this.achieveLevel = request.getAchieveLevel();
            this.testStatus = TestStatus.PUTANSWER;
            this.addFile = request.getAddFile();
            this.dateRegister = LocalDateTime.now();
        }
        @Override
        public AnswerTest build() {
            return new AnswerTest(this);
        }
    }

}
