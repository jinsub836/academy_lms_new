package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.admin.AdminInfoChangeRequest;
import org.bj.academylmsapi.model.admin.AdminRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

// 관리자 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Admin implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 관리자이름
    @Column(nullable = false, length = 20)
    private String adminName;

    // 관리자 아이디
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    // 관리자 비밀번호
    @Column(nullable = false, columnDefinition = "TEXT")
    private String password;

    // 등급
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberType memberType;

    // 성별
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 본인 휴대폰
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 가입일
    @Column(nullable = false)
    private LocalDateTime dateJoin;

    public void putAdmin(AdminInfoChangeRequest request) {
        this.adminName = request.getAdminName();
        this.username = request.getUsername();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.phoneNumber = request.getPhoneNumber();
    }

    private Admin(Builder builder) {
        this.adminName = builder.adminName;
        this.username = builder.username;
        this.password = builder.password;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.memberType = builder.memberType;
        this.phoneNumber = builder.phoneNumber;
        this.dateJoin = builder.dateJoin;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static class Builder implements CommonModelBuilder<Admin> {
        private final String adminName;
        private final String username;
        private final String password;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final MemberType memberType;
        private final String phoneNumber;
        private final LocalDateTime dateJoin;

        public Builder(AdminRequest request, MemberType memberType) {
            this.adminName = request.getAdminName();
            this.username = request.getUsername()+"_admin";
            this.password = request.getPassword();
            this.gender = request.getGender();
            this.dateBirth = request.getDateBirth();
            this.memberType = MemberType.ROLE_MANAGEMENT;
            this.phoneNumber = request.getPhoneNumber();
            this.dateJoin = LocalDateTime.now();
        }

        @Override
        public Admin build() {
            return new Admin(this);
        }
    }
}
