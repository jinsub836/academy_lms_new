package org.bj.academylmsapi.entity.timeTable;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class TimeTable {
    // id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 과정 id
    @JoinColumn(nullable = false, name = "subjectId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Subject subjectId;

    // 훈련일자
    @Column(nullable = false, length = 8)
    private String dateTraining;

    // 훈련시작시간
    @Column(nullable = false, length = 4)
    private String startTimeTrain;

    // 훈련종료시간
    @Column(nullable = false, length = 4)
    private String endTime;

    // 방학/원격여부 (일단 String으로 놔둠)
    private String isRemote;

    // 시작시간
    @Column(nullable = false, length = 4)
    private String startTime;

    // 시간구분
    @Column(nullable = false, length = 1)
    private String timeSorted;

    // 훈련강사코드
    @Column(length = 20)
    private String teacherCode;

    // 교육장소(강의실)코드
    @Column(length = 30)
    private String placeCode;

    // 교과목(및 능력단위)코드
    @Column(length = 30)
    private String subjectCode;


    private TimeTable(Builder build){
       this.subjectId = build.subjectId;
       this.dateTraining = build.dateTraining;
       this.startTimeTrain = build.startTimeTrain;
       this.endTime = build.endTime;
       this.isRemote = build.isRemote;
       this.startTime = build.startTime;
       this.timeSorted = build.timeSorted;
       this.placeCode  = build.placeCode;
       this.subjectCode = build.subjectCode;
       this.teacherCode = build.teacherCode;
    }

    public static class Builder implements CommonModelBuilder<TimeTable>{
        private final Subject subjectId;
        private final String dateTraining;
        private final String startTimeTrain;
        private final String endTime;
        private final String isRemote;
        private final String startTime;
        private final String timeSorted;
        private final String placeCode;
        private final String subjectCode;
        private final String teacherCode;

        public Builder(String[] data, Subject subjectId){
            this.subjectId = subjectId;
            this.dateTraining = data[0];
            this.startTimeTrain = data[1];
            this.endTime = data[2];
            this.isRemote = data[3];
            this.startTime = data[4];
            this.timeSorted = data[5];
            this.teacherCode = data[6];
            this.placeCode = data[7];
            this.subjectCode = data[8] == null ? null : data[8];
    }

        @Override
        public TimeTable build() {
            return new TimeTable(this);
        }
    }


}
