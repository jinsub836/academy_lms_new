package org.bj.academylmsapi.entity.trainingManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.trainingManagement.trainingRegister.TrainingRegisterRequest;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;

// 훈련관리 - 훈련일지 등록 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingRegister {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 강사id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teacherId")
    private Teacher teacher;
    // 훈련날짜
    @Column(nullable = false)
    private LocalDateTime dateTraining;
    // 재적
    @Column(nullable = false)
    private Short registrationNum;
    // 출석
    private Short attendanceNum;
    // 결석
    private String absentWho;
    // 지각
    private String lateWho;
    // 조퇴
    private String earlyLeaveWho;
    // 훈련과목
    @Column(length = 20)
    private String trainingSubject;
    // 훈련내용
    @Column(length = 30)
    private String trainingContent;
    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putTrainingRegister(TrainingRegisterRequest request, Teacher teacher) {
        this.teacher = teacher;
        this.dateTraining = request.getDateTraining();
        this.registrationNum = request.getRegistrationNum();
        this.attendanceNum = request.getAttendanceNum();
        this.absentWho = request.getAbsentWho();
        this.lateWho = request.getLateWho();
        this.earlyLeaveWho = request.getEarlyLeaveWho();
        this.trainingSubject = request.getTrainingSubject();
        this.trainingContent = request.getTrainingContent();
    }

    private TrainingRegister(Builder builder) {
        this.teacher = builder.teacher;
        this.dateTraining = builder.dateTraining;
        this.registrationNum = builder.registrationNum;
        this.attendanceNum = builder.attendanceNum;
        this.absentWho = builder.absentWho;
        this.lateWho = builder.lateWho;
        this.earlyLeaveWho = builder.earlyLeaveWho;
        this.trainingSubject = builder.trainingSubject;
        this.trainingContent = builder.trainingContent;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingRegister> {
        private final Teacher teacher;
        private final LocalDateTime dateTraining;
        private final Short registrationNum;
        private final Short attendanceNum;
        private final String absentWho;
        private final String lateWho;
        private final String earlyLeaveWho;
        private final String trainingSubject;
        private final String trainingContent;
        private final LocalDateTime dateRegister;

        public Builder(TrainingRegisterRequest request, Teacher teacher) {
            this.teacher = teacher;
            this.dateTraining = request.getDateTraining();
            this.registrationNum = request.getRegistrationNum();
            this.attendanceNum = request.getAttendanceNum();
            this.absentWho = request.getAbsentWho();
            this.lateWho = request.getLateWho();
            this.earlyLeaveWho = request.getEarlyLeaveWho();
            this.trainingSubject = request.getTrainingSubject();
            this.trainingContent = request.getTrainingContent();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public TrainingRegister build() {
            return new TrainingRegister(this);
        }
    }
}
