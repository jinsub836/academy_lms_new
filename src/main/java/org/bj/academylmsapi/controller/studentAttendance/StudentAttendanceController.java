package org.bj.academylmsapi.controller.studentAttendance;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceItem;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceResponse;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// 수강생 출결 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/studentAttendance")
@Tag(name = "StudentAttendance", description = "수강생 출결")
public class StudentAttendanceController {
    private final StudentAttendanceService studentAttendanceService;

    // 수강생 출결 C
    @PostMapping("/new")
    @Operation(summary = "수강생 출결 등록")
    public CommonResult setStudentAttendance(@RequestBody StudentAttendanceRequest request) {
        studentAttendanceService.SetStudentAttendance(request.getStudentId());
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 단수 R
    @GetMapping("/detail")
    @Operation(summary = "수강생 출결 리스트 보기")
    public SingleResult<StudentAttendanceResponse> getStudentAttendancesRate() {
        return ResponseService.getSingleResult(studentAttendanceService.getStudentAttendancesRate());
    }

    // 수강생 출결 복수 R
    @GetMapping("/all/")
    @Operation(summary = "수강생 출결 리스트 보기")
    public ListResult<StudentAttendanceItem> getStudentAttendances() {
        return ListConvertService.settingListResult(studentAttendanceService.getStudentAttendances());
    }

    // 수강생 출결 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "수강생 출결 리스트 보기 (페이징)")
    public ListResult<StudentAttendanceItem> getStudentAttendancesPage(@PathVariable int pageNum) {
        return studentAttendanceService.getStudentAttendancesPage(pageNum);
    }

    // 수강생 출결 U
    @PutMapping("/changeInfo/studentAttendanceId/{studentAttendanceId}")
    @Operation(summary = "수강생 출결 정보 수정")
    public CommonResult putStudentAttendance(@PathVariable long studentAttendanceId, @RequestBody StudentAttendanceRequest request) {
        studentAttendanceService.putStudentAttendance(studentAttendanceId, request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 D
    @DeleteMapping("/delete/studentAttendanceId/{studentAttendanceId}")
    @Operation(summary = "수강생 출결 정보 삭제")
    public CommonResult delStudentAttendance(@PathVariable long studentAttendanceId) {
        studentAttendanceService.delStudentAttendance(studentAttendanceId);
        return ResponseService.getSuccessResult();
    }
}
