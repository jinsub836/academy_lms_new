package org.bj.academylmsapi.controller.studentAttendance;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalFixRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalItem;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceApprovalService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// 수강생 출결 승인 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/student-attendance-approval")
@Tag(name = "StudentAttendanceApproval", description = "수강생 출결 승인")
public class StudentAttendanceApprovalController {
    private final StudentAttendanceApprovalService studentAttendanceApprovalService;

    /**
     * 수강생 출결 승인 등록
     */
    @PostMapping("/new")
    @Operation(summary = "수강생 출결 승인 등록")
    public CommonResult setStudentAttendanceApproval(@RequestBody StudentAttendanceApprovalRequest request) {

        studentAttendanceApprovalService.setStudentAttendanceApproval(request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 승인 R
    @GetMapping("/all")
    @Operation(summary = "수강생 출결 승인 리스트")
    public ListResult<StudentAttendanceApprovalItem> getStudentAttendanceApprovals() {
        return ListConvertService.settingListResult(studentAttendanceApprovalService.getStudentAttendanceApprovals());
    }

    // 수강생 출결 승인 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "수강생 출결 승인 리스트 (페이징)")
    public ListResult<StudentAttendanceApprovalItem> getStudentAttendanceApprovalsPage(@PathVariable int pageNum) {
        return studentAttendanceApprovalService.getStudentAttendanceApprovalsPage(pageNum);
    }


    // 수강생 출결 승인 정보 수정 U
    @PutMapping("/changeInfo/student-attendance-approvalId/{id}")
    @Operation(summary = "수강생 출결 승인 정보 수정")
    public CommonResult putStudentAttendanceApproval(@PathVariable long id, @RequestBody StudentAttendanceApprovalFixRequest request) {
        studentAttendanceApprovalService.putStudentAttendance(id, request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 승인 U
    @PutMapping("/change-approval/student-attendance-approvalId/{id}")
    @Operation(summary = "수강생 출결 승인 ")
    public CommonResult putStudentAttendanceApprovalType(@PathVariable long id) {
        studentAttendanceApprovalService.putStudentAttendanceApprovalAdmin(id);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 승인 D
    @DeleteMapping("/delete/student-attendance-approvalId/{id}")
    @Operation(summary = "수강생 출결 승인 정보 삭제")
    public CommonResult delStudentAttendanceApproval(@PathVariable long id) {
        studentAttendanceApprovalService.delStudentAttendanceApproval(id);
        return ResponseService.getSuccessResult();
    }
}
