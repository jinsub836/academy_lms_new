package org.bj.academylmsapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.student.*;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.StudentService;
import org.springframework.web.bind.annotation.*;

// 수강생 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/student")
@Tag(name = "Student", description = "수강생")
public class StudentController {
    private final StudentService studentService;

    // 수강생 C
    @PostMapping("/new")
    @Operation(summary = "수강생 등록")
    public CommonResult setStudent(@RequestBody @Valid StudentRequest request) {
        studentService.setStudent(request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 복수 R
    @GetMapping("/all")
    @Operation(summary = "수강생 리스트 전체보기")
    public ListResult<StudentItem> getStudentsList() {
        return ListConvertService.settingListResult(studentService.getStudents());
    }

   // 수강생 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "수강생 리스트 (페이징)")
    public ListResult<StudentItem> getStudentsPage(@PathVariable int pageNum) {
        return studentService.getStudentsPage(pageNum);
    }

    // 수강생 단수 R
    @GetMapping("/detail")
    @Operation(summary = "수강생 상세보기")
    public SingleResult<StudentResponse> getStudent() {
        return ResponseService.getSingleResult(studentService.getStudent());
    }

    // 수강생 U
    @PutMapping("/changeInfo")
    @Operation(summary = "수강생 정보 수정")
    public CommonResult putStudent(@RequestBody @Valid StudentInfoChangeRequest studentInfoChangeRequest) {
        studentService.putStudent(studentInfoChangeRequest);
        return ResponseService.getSuccessResult();
    }

    // 수강생 U 관리자 버전
    @PutMapping("/changeInfoAdmin")
    @Operation(summary = "수강생 정보 수정 관리자용")
    public CommonResult putStudentAdmin(@RequestBody @Valid StudentInfoChangeAdminRequest studentInfoChangeAdminRequest) {
        studentService.putStudentAdmin(studentInfoChangeAdminRequest);
        return ResponseService.getSuccessResult();
    }

    // 수강생 U 비밀번호
    @PutMapping("/changePassword")
    @Operation(summary = "수강생 비밀번호 수정")
    public CommonResult putStudentPassword(@RequestBody @Valid StudentPasswordChangeRequest studentPasswordChangeRequest) {
        studentService.putStudentPassword(studentPasswordChangeRequest);
        return ResponseService.getSuccessResult();
    }


    // 수강생 D
    @DeleteMapping("/delete")
    @Operation(summary = "수강생 정보 삭제")
    public CommonResult delStudent() {
        studentService.delStudent();
        return ResponseService.getSuccessResult();
    }
}
