package org.bj.academylmsapi.controller;

import org.bj.academylmsapi.exception.CAccessDeniedException;
import org.bj.academylmsapi.exception.CEntryPointException;
import org.bj.academylmsapi.model.result.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("entry-point")
    public CommonResult entryPointException() {
        throw new CEntryPointException();
    }
}
