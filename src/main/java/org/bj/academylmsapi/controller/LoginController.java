package org.bj.academylmsapi.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.model.login.LoginRequest;
import org.bj.academylmsapi.model.login.LoginResponse;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.AdminRepository;
import org.bj.academylmsapi.service.AdminService;
import org.bj.academylmsapi.service.LoginService;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdminWeb (@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doAdminLogin(MemberType.ROLE_MANAGEMENT,loginRequest,"WEB"));
    }

    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUserAPP(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doStudentLogin(MemberType.ROLE_GENERAL,loginRequest,"APP"));
    }
    @PostMapping("/web/user")
    public SingleResult<LoginResponse> doLoginUserWeb(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doStudentLogin(MemberType.ROLE_GENERAL,loginRequest,"WEB"));
    }

    @PostMapping("/app/teacher")
    public SingleResult<LoginResponse> doLonginTeacherAPP(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doTeacherLogin(MemberType.ROLE_TEACHER,loginRequest,"APP"));
    }

    @PostMapping("/web/teacher")
    public SingleResult<LoginResponse> doLonginTeacherWeb(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doTeacherLogin(MemberType.ROLE_TEACHER,loginRequest,"WEB"));
    }


}
