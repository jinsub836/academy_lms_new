package org.bj.academylmsapi.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Schema(description = "NOANSWER = 응시 전, PUTANSWER = 응시완료 (서명 전), SIGNOKAY = 응시완료 (서명 완료)")
public enum TestStatus {
    NOANSWER("응시 전"),
    PUTANSWER("응시완료 (서명 필요)"),
    SIGNOKAY("응시완료 (서명 완료)");

    private final String TestStatus;

}
