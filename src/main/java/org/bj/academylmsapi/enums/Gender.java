package org.bj.academylmsapi.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

// 성별 enum

@AllArgsConstructor
@Getter
@Schema(description = "MAN = 남자 WOMAN = 여자")
public enum Gender {

    MAN("남자"),
    WOMAN("여자");

    private final String genderName;
}
