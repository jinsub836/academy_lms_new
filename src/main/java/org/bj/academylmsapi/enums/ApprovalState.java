package org.bj.academylmsapi.enums;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

// 승인상태 Enum

@AllArgsConstructor
@Getter
@Schema(description = "NOTAPPROVED = 미승인, TEACHERAPPROVAL = 강사승인, ADMINAPPROVAL = 관리자승인")
public enum ApprovalState {
    NOTAPPROVED("미승인"),
    TEACHERAPPROVAL("강사승인"),
    ADMINAPPROVAL("관리자승인");

    private final String approvalStateName;
}
