package org.bj.academylmsapi.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

// 수강상태 enum
@AllArgsConstructor
@Getter
@Schema(description = "INCLASS = 수강, OUTCLASS = 수강포기, WAITCLASS = 대기중")
public enum StudentStatus {
    INCLASS("수강"),
    OUTCLASS("수강포기"),
    WAITCLASS("대기중");

    private final String studentStatusName;
}
