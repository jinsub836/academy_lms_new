package org.bj.academylmsapi.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Schema(description = "ROLE_GENERAL = 일반회원, ROLE_TEACHER = 강사, ROLE_MANAGEMENT = 관리자")
public enum MemberType {
    ROLE_GENERAL("일반회원"),
    ROLE_TEACHER("강사"),
    ROLE_MANAGEMENT("관리자");

    private final String role;
}
