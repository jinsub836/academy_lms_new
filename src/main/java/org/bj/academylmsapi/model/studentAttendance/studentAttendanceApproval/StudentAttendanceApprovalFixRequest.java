package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 수강생 출결 승인 C model

@Getter
@Setter
public class StudentAttendanceApprovalFixRequest {
    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "시작일")
    private LocalDate dateStart;

    @Schema(description = "종료일")
    private LocalDate dateEnd;

    @Schema(description = "사유", maxLength = 30)
    @Length(min = 2, max = 30)
    private String reason;

    @Schema(description = "첨부파일 주소")
    private String addFile;

    @Schema(description = "출결승인 유형")
    private AttendanceType attendanceType;

    @Schema(description = "승인 상태")
    private ApprovalState approvalState;
}
