package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 수강생 출결 승인 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceApprovalItem {
    @Schema(description = "수강생 출결 승인 id")
    private Long id;

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "출결승인 유형")
    private AttendanceType attendanceType;

    @Schema(description = "사유")
    private String reason;

    @Schema(description = "시작일")
    private LocalDate dateStart;

    @Schema(description = "종료일")
    private LocalDate dateEnd;

    @Schema(description = "승인상태")
    private ApprovalState approvalState;

    @Schema(description = "등록일")
    private LocalDate dateRegister;

    private StudentAttendanceApprovalItem(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.attendanceType = builder.attendanceType;
        this.reason = builder.reason;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.approvalState = builder.approvalState;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApprovalItem> {
        private final Long id;
        private final Long studentId;
        private final AttendanceType attendanceType;
        private final String reason;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final ApprovalState approvalState;
        private final LocalDate dateRegister;

        public Builder(StudentAttendanceApproval studentAttendanceApproval) {
            this.id = studentAttendanceApproval.getId();
            this.studentId = studentAttendanceApproval.getStudent().getId();
            this.attendanceType = studentAttendanceApproval.getAttendanceType();
            this.reason = studentAttendanceApproval.getReason();
            this.dateStart = studentAttendanceApproval.getDateStart();
            this.dateEnd = studentAttendanceApproval.getDateEnd();
            this.approvalState = studentAttendanceApproval.getApprovalState();
            this.dateRegister = studentAttendanceApproval.getDateRegister();
        }

        @Override
        public StudentAttendanceApprovalItem build() {
            return new StudentAttendanceApprovalItem(this);
        }
    }
}
