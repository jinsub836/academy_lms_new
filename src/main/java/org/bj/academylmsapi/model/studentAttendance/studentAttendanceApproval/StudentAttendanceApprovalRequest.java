package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.AttendanceType;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 수강생 출결 승인 C model

@Getter
@Setter
public class StudentAttendanceApprovalRequest {
    @NotNull
    @Schema(description = "시작일")
    private String dateStart;

    @NotNull
    @Schema(description = "종료일")
    private String dateEnd;

    @NotNull
    @Schema(description = "사유", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String reason;

    @Schema(description = "첨부파일 주소")
    private String addFile;

    @NotNull
    @Schema(description = "출결승인 유형")
    private AttendanceType attendanceType;
}
