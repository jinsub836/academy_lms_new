package org.bj.academylmsapi.model.studentAttendance.studentAttendance;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

// 수강생 출결 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceResponse {
    @Schema(description = "출석", maxLength = 5)
    @Length(max = 5)
    private Integer studentAttendence;

    @Schema(description = "조퇴", maxLength = 5)
    @Length(max = 5)
    private Integer studentOut;

    @NotNull
    @Schema(description = "지각", maxLength = 5)
    @Length(max = 5)
    private Integer studentLate;

    @Schema(description = "무단결석", maxLength = 5)
    @Length(max = 5)
    private Integer studentAbsence;

    @Schema(description = "출석률", maxLength = 5)
    @Length(max = 5)
    private Double attendanceRate;

    @Schema(description = "병가", maxLength = 5)
    @Length(max = 5)
    private Integer studentSick;

    private StudentAttendanceResponse(Builder builder) {
        this.studentAttendence = builder.studentAttendence;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentAbsence = builder.studentAbsence;
        this.attendanceRate = builder.attendanceRate;
        this.studentSick = builder.studentSick;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceResponse> {
        private final Integer studentAttendence;
        private final Integer studentOut;
        private final Integer studentLate;
        private final Integer studentAbsence;
        private final Double attendanceRate;
        private final Integer studentSick;

        public Builder(StudentAttendance studentAttendance) {
            int studentReal =  studentAttendance.getStudentAttendence() - studentAttendance.getStudentAbsence()
                    -studentAttendance.getStudentLate()/3 -studentAttendance.getStudentOut()/3;
            this.studentAttendence = studentReal;
            this.studentOut = studentAttendance.getStudentOut();
            this.studentLate = studentAttendance.getStudentLate();
            this.studentAbsence = studentAttendance.getStudentAbsence();
            this.attendanceRate = (double) studentReal / (studentAttendance.getStudentAttendence());
            this.studentSick = studentAttendance.getStudentSick();
        }
        @Override
        public StudentAttendanceResponse build() {
            return new StudentAttendanceResponse(this);
        }
    }
}
