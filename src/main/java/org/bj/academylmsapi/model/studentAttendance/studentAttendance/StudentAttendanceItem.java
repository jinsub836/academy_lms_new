package org.bj.academylmsapi.model.studentAttendance.studentAttendance;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

// 수강생 출결 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceItem {
    @Schema(description = "수강생 출결 id")
    private Long id;

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "출석", maxLength = 5)
    @Length(max = 5)
    private Integer studentAttendence;

    @Schema(description = "조퇴", maxLength = 5)
    @Length(max = 5)
    private Integer studentOut;

    @NotNull
    @Schema(description = "지각", maxLength = 5)
    @Length(max = 5)
    private Integer studentLate;

    @Schema(description = "무단결석", maxLength = 5)
    @Length(max = 5)
    private Integer studentAbsence;

    @NotNull
    @Schema(description = "병가", maxLength = 5)
    @Length(max = 5)
    private Integer studentSick;


    private StudentAttendanceItem(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.studentAttendence = builder.studentAttendence;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentAbsence = builder.studentAbsence;
        this.studentSick = builder.studentSick;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceItem> {
        private final Long id;
        private final Long studentId;
        private final Integer studentAttendence;
        private final Integer studentOut;
        private final Integer studentLate;
        private final Integer studentAbsence;
        private final Integer studentSick;

        public Builder(StudentAttendance studentAttendance) {
            this.id = studentAttendance.getId();
            this.studentId = studentAttendance.getStudent().getId();
            this.studentAttendence = studentAttendance.getStudentAttendence();
            this.studentOut = studentAttendance.getStudentOut();
            this.studentLate = studentAttendance.getStudentLate();
            this.studentAbsence = studentAttendance.getStudentAbsence();
            this.studentSick = studentAttendance.getStudentSick();

        }

        @Override
        public StudentAttendanceItem build() {
            return new StudentAttendanceItem(this);
        }
    }
}
