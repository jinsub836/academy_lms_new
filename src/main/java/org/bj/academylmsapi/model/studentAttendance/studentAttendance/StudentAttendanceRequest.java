package org.bj.academylmsapi.model.studentAttendance.studentAttendance;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

// 수강생 출결 C model

@Getter
@Setter
public class StudentAttendanceRequest {
    @NotNull
    @Schema(description = "수강생 id")
    private Long studentId;

    @NotNull
    @Schema(description = "출석", maxLength = 5)
    @Length(max = 5)
    private Integer studentAttendence;

    @NotNull
    @Schema(description = "조퇴", maxLength = 5)
    @Length(max = 5)
    private Integer studentOut;

    @NotNull
    @Schema(description = "지각", maxLength = 5)
    @Length(max = 5)
    private Integer studentLate;

    @NotNull
    @Schema(description = "무단결석", maxLength = 5)
    @Length(max = 5)
    private Integer studentAbsence;

    @NotNull
    @Schema(description = "병가", maxLength = 5)
    @Length(max = 5)
    private Integer studentSick;
}
