package org.bj.academylmsapi.model.board.reply;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.MemberType;
import org.hibernate.validator.constraints.Length;

// 게시판 - 답변 C model

@Getter
@Setter
public class ReplyRequest {
    @NotNull
    @Schema(description = "게시판 id")
    private Long boardId;

    @NotNull
    @Schema(description = "회원등급")
    private MemberType memberType;

    @NotNull
    @Schema(description = "멤버 id(수강생 및 강사)")
    private Long memberId;

    @NotNull
    @Schema(description = "답변 제목", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String boardReTitle;

    @NotNull
    @Schema(description = "답변 작성자")
    private String username;

    @NotNull
    @Schema(description = "답변 내용")
    private String boardContent;
}
