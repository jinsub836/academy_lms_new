package org.bj.academylmsapi.model.board.reply;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.entity.board.Reply;
import org.bj.academylmsapi.enums.MemberType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 게시판 - 답변 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReplyResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "게시판 id")
    private Long boardId;

    @Schema(description = " 회원등급")
    private MemberType memberType;

    @Schema(description = "멤버 id")
    private Long memberId;

    @Schema(description = "답변 제목")
    private String boardReTitle;

    @Schema(description = "답변 작성자")
    private String username;

    @Schema(description = "답변 내용")
    private String boardContent;

    @Schema(description = "답변 등록일")
    private LocalDateTime dateCreate;

    private ReplyResponse(Builder builder) {
        this.id = builder.id;
        this.boardId = builder.boardId;
        this.memberType = builder.memberType;
        this.memberId = builder.memberId;
        this.boardReTitle = builder.boardReTitle;
        this.username = builder.username;
        this.boardContent = builder.boardContent;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<ReplyResponse> {
        private final Long id;
        private final Long boardId;
        private final MemberType memberType;
        private final Long memberId;
        private final String boardReTitle;
        private final String username;
        private final String boardContent;
        private final LocalDateTime dateCreate;

        public Builder(Reply reply) {
            this.id = reply.getId();
            this.boardId = reply.getBoard().getId();
            this.memberType = reply.getMemberType();
            this.memberId = reply.getMemberId();
            this.boardReTitle = reply.getBoardReTitle();
            this.username = reply.getUsername();
            this.boardContent = reply.getBoardContent();
            this.dateCreate = reply.getDateCreate();
        }

        @Override
        public ReplyResponse build() {
            return new ReplyResponse(this);
        }
    }
}
