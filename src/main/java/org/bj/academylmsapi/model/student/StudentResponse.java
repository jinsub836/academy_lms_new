package org.bj.academylmsapi.model.student;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.FinalEducation;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.StudentStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 수강생 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "과정명")
    private String subjectName;

    @Schema(description = "사진 주소")
    private String imgSrc;

    @Schema(description = "이름")
    private String studentName;

    @Schema(description = "성별")
    private Gender gender;

    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @Schema(description = "본인 휴대폰번호")
    private String phoneNumber;

    @Schema(description = "주민등록번호")
    private String idNumber;

    @Schema(description = "주소")
    private String address;

    @Schema(description = "이메일")
    private String email;

    @Schema(description = "최종 학력", enumAsRef = true)
    private FinalEducation finalEducation;

    @Schema(description = "전공")
    private String major;

    @Schema(description = "수강상태", enumAsRef = true)
    private StudentStatus studentStatus;

    @Schema(description = "상태 변경일")
    private LocalDate dateStatusChange;

    @Schema(description = "상태 변경 사유")
    private String statusWhy;

    private StudentResponse(Builder builder) {
        this.id = builder.id;
        this.subjectName = builder.subjectName;
        this.imgSrc = builder.imgSrc;
        this.studentName = builder.studentName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.idNumber = builder.idNumber;
        this.address = builder.address;
        this.email = builder.email;
        this.finalEducation = builder.finalEducation;
        this.major = builder.major;
        this.studentStatus = builder.studentStatus;
        this.dateStatusChange = builder.dateStatusChange;
        this.statusWhy = builder.statusWhy;
    }

    public static class Builder implements CommonModelBuilder<StudentResponse> {
        private final Long id;
        private final String subjectName;
        private final String imgSrc;
        private final String studentName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String idNumber;
        private final String address;
        private final String email;
        private final FinalEducation finalEducation;
        private final String major;
        private final StudentStatus studentStatus;
        private final LocalDate dateStatusChange;
        private final String statusWhy;

        public Builder(Student student) {
            this.id = student.getId();
            this.subjectName = student.getSubject().getSubjectName();
            this.imgSrc = student.getImgSrc();
            this.studentName = student.getStudentName();
            this.gender = student.getGender();
            this.dateBirth = student.getDateBirth();
            this.phoneNumber = student.getPhoneNumber();
            this.idNumber = student.getIdNumber();
            this.address = student.getAddress();
            this.email = student.getEmail();
            this.finalEducation = student.getFinalEducation();
            this.major = student.getMajor();
            this.studentStatus = student.getStudentStatus();
            this.dateStatusChange = student.getDateStatusChange();
            this.statusWhy = student.getStatusWhy();
        }


        @Override
        public StudentResponse build() {
            return new StudentResponse(this);
        }
    }
}
