package org.bj.academylmsapi.model.student;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.FinalEducation;
import org.hibernate.validator.constraints.Length;

// 수강생 email U model

@Getter
@Setter
public class StudentInfoChangeRequest {
    @Schema(description = "사진 주소")
    private String imgSrc;

    @NotNull
    @Schema(description = "본인 휴대폰번호", minLength = 13, maxLength = 13)
    @Length(max = 13)
    private String phoneNumber;

    @NotNull
    @Schema(description = "주소", minLength = 10, maxLength = 100)
    @Length(min = 10, max = 100)
    private String address;

    @NotNull
    @Schema(description = "이메일" , maxLength = 50)
    @Length(max = 50)
    private String email;

    @NotNull
    @Schema(description = "최종 학력", enumAsRef = true)
    private FinalEducation finalEducation;

    @NotNull
    @Schema(description = "전공", minLength = 6, maxLength = 20)
    @Length(min = 6, max = 20)
    private String major;

    @NotNull
    @Schema(description = "수강생 아이디" ,minLength = 5, maxLength = 20)
    @Length(min = 5, max = 20)
    private String username;

    @NotNull
    @Schema(description = "수강생 비밀번호" , minLength = 8)
    @Length(min = 8)
    private String password;
}
