package org.bj.academylmsapi.model.student;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.FinalEducation;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.StudentStatus;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 email U model

@Getter
@Setter
public class StudentInfoChangeAdminRequest {
    @NotNull
    @Schema(description = "과정 id")
    private Long subjectChoiceId;

    @Schema(description = "사진 주소")
    private String imgSrc;

    @NotNull
    @Schema(description = "이름", minLength = 2, maxLength = 20)
    @Length(min = 2, max = 20)
    private String studentName;

    @NotNull
    @Schema(description = "수강생 아이디", minLength = 5, maxLength = 20)
    @Length(min = 5, max = 20)
    private String username;

    @NotNull
    @Schema(description = "비밀번호", minLength = 8)
    @Length(min = 8)
    private String password;

    @NotNull
    @Schema(description = "성별")
    private Gender gender;

    @NotNull
    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @NotNull
    @Schema(description = "본인 휴대폰번호", minLength = 13, maxLength = 13)
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @NotNull
    @Schema(description = "주민등록번호", minLength = 14, maxLength = 14)
    @Length(min = 14, max = 14)
    private String idNumber;

    @NotNull
    @Schema(description = "주소", minLength = 10, maxLength = 100)
    @Length(min = 10, max = 100)
    private String address;

    @NotNull
    @Schema(description = "이메일" , maxLength = 50)
    @Length(max = 50)
    private String email;

    @NotNull
    @Schema(description = "최종 학력", enumAsRef = true)
    private FinalEducation finalEducation;

    @NotNull
    @Schema(description = "전공", minLength = 6, maxLength = 20)
    @Length(min = 6, max = 20)
    private String major;

    @NotNull
    @Schema(description = "수강상태", enumAsRef = true)
    private StudentStatus studentStatus;

    @NotNull
    @Schema(description = "상태 변경일")
    private LocalDate dateStatusChange;

    @NotNull
    @Schema(description = "상태 변경 사유")
    private String statusWhy;


}
