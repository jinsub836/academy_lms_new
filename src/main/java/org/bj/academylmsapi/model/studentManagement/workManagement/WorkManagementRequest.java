package org.bj.academylmsapi.model.studentManagement.workManagement;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 취업관리사항 C model

@Getter
@Setter
public class WorkManagementRequest {
    @NotNull
    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "업체명", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String companyName;

    @Schema(description = "사업자등록번호", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String businessRegisterNumber;

    @Schema(description = "취업확인서", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String employmentConfirmationForm;

    @Schema(description = "회사주소", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String companyAddress;

    @Schema(description = "회사연락처", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String companyCallNumber;

    @Schema(description = "근무직종", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String workField;

    @Schema(description = "고용보험유무", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String employmentInsuranceStatus;

    @Schema(description = "취업일")
    private LocalDate dateEmployment;

}
