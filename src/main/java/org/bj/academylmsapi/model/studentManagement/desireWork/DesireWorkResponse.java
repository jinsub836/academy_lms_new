package org.bj.academylmsapi.model.studentManagement.desireWork;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.DesireWork;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

// 학생관리 - 취업관리 - 희망근무조건 단수 R model

@Getter
@NoArgsConstructor
public class DesireWorkResponse {
    @NotNull
    @Schema(description = "id")
    private Long id;

    @NotNull
    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "희망지역")
    private String hopeArea;

    @Schema(description = "취업희망분야")
    private String hopeWorkField;

    @Schema(description = "희망급여수준")
    private String hopePayLevel;

    @Schema(description = "가능고용형태")
    private String possibleEmployForm;

    @Schema(description = "가능근무형태")
    private String possibleWorkForm;

    @Schema(description = "가능근무시간")
    private String possibleWorkTime;

    @Schema(description = "수강생 요청사항")
    private String studentRequest;

    private DesireWorkResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.hopeArea = builder.hopeArea;
        this.hopeWorkField = builder.hopeWorkField;
        this.hopePayLevel = builder.hopePayLevel;
        this.possibleEmployForm = builder.possibleEmployForm;
        this.possibleWorkForm = builder.possibleWorkForm;
        this.possibleWorkTime = builder.possibleWorkTime;
        this.studentRequest = builder.studentRequest;
    }

    public static class Builder implements CommonModelBuilder<DesireWorkResponse> {
        private final Long id;
        private final Long studentId;
        private final String hopeArea;
        private final String hopeWorkField;
        private final String hopePayLevel;
        private final String possibleEmployForm;
        private final String possibleWorkForm;
        private final String possibleWorkTime;
        private final String studentRequest;

        public Builder(DesireWork desireWork) {
            this.id = desireWork.getId();
            this.studentId = desireWork.getStudent().getId();
            this.hopeArea = desireWork.getHopeArea();
            this.hopeWorkField = desireWork.getHopeWorkField();
            this.hopePayLevel = desireWork.getHopePayLevel();
            this.possibleEmployForm = desireWork.getPossibleEmployForm();
            this.possibleWorkForm = desireWork.getPossibleWorkForm();
            this.possibleWorkTime = desireWork.getPossibleWorkTime();
            this.studentRequest = desireWork.getStudentRequest();
        }

        @Override
        public DesireWorkResponse build() {
            return new DesireWorkResponse(this);
        }
    }
}
