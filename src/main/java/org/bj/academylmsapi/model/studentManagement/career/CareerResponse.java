package org.bj.academylmsapi.model.studentManagement.career;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.Career;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 경력사항 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CareerResponse {
    @NotNull
    @Schema(description = "id")
    private Long id;

    @NotNull
    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "근무처")
    private String office;

    @Schema(description = "근무기간")
    private String dateWork;

    @Schema(description = "직위")
    private String position;

    @Schema(description = "담당업무")
    private String whatTask;

    @Schema(description = "급여수준")
    private String payLevel;

    @Schema(description = "보유자격증")
    private String myLicense;

    @Schema(description = "고용지원금 대상자 여부")
    private String employSupport;

    @Schema(description = "소속기간명")
    private String belongOrganization;

    @Schema(description = "워크넷 취업활동 여부")
    private String workNetJobSearchStatus;

    @Schema(description = "보유기술")
    private String haveSkill;

    @Schema(description = "기타특이사항")
    private String etc;

    private CareerResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.office = builder.office;
        this.dateWork = builder.dateWork;
        this.position = builder.position;
        this.whatTask = builder.whatTask;
        this.payLevel = builder.payLevel;
        this.myLicense = builder.myLicense;
        this.employSupport = builder.employSupport;
        this.belongOrganization = builder.belongOrganization;
        this.workNetJobSearchStatus = builder.workNetJobSearchStatus;
        this.haveSkill = builder.haveSkill;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<CareerResponse> {
        private final Long id;
        private final Long studentId;
        private final String office;
        private final String dateWork;
        private final String position;
        private final String whatTask;
        private final String payLevel;
        private final String myLicense;
        private final String employSupport;
        private final String belongOrganization;
        private final String workNetJobSearchStatus;
        private final String haveSkill;
        private final String etc;

        public Builder(Career career) {
            this.id = career.getId();
            this.studentId = career.getStudent().getId();
            this.office = career.getOffice();
            this.dateWork = career.getDateWork();
            this.position = career.getPosition();
            this.whatTask = career.getWhatTask();
            this.payLevel = career.getPayLevel();
            this.myLicense = career.getMyLicense();
            this.employSupport = career.getEmploySupport();
            this.belongOrganization = career.getBelongOrganization();
            this.workNetJobSearchStatus = career.getWorkNetJobSearchStatus();
            this.haveSkill = career.getHaveSkill();
            this.etc = career.getEtc();
        }
        @Override
        public CareerResponse build() {
            return new CareerResponse(this);
        }
    }
}
