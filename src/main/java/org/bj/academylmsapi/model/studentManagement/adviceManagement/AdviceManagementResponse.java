package org.bj.academylmsapi.model.studentManagement.adviceManagement;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.AdviceManagement;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 상담관리 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdviceManagementResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "강사 id")
    private Long teacherId;

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "수강생명")
    private String studentName;

    @Schema(description = "상담제목")
    private String adviceTitle;

    @Schema(description = "상담날짜")
    private LocalDate dateAdvice;

    @Schema(description = "상담내용")
    private String adviceContent;

    @Schema(description = "등록일")
    private LocalDateTime dateRegister;

    @Schema(description = "비고")
    private String etc;

    private AdviceManagementResponse(Builder builder) {
        this.id = builder.id;
        this.teacherId = builder.teacherId;
        this.studentId = builder.studentId;
        this.studentName = builder.studentName;
        this.adviceTitle = builder.adviceTitle;
        this.dateAdvice = builder.dateAdvice;
        this.adviceContent = builder.adviceContent;
        this.dateRegister = builder.dateRegister;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<AdviceManagementResponse> {
        private final Long id;
        private final Long teacherId;
        private final Long studentId;
        private final String studentName;
        private final String adviceTitle;
        private final LocalDate dateAdvice;
        private final String adviceContent;
        private final LocalDateTime dateRegister;
        private final String etc;

        public Builder(AdviceManagement adviceManagement) {
            this.id = adviceManagement.getId();
            this.teacherId = adviceManagement.getTeacher().getId();
            this.studentId = adviceManagement.getStudent().getId();
            this.studentName = adviceManagement.getStudentName();
            this.adviceTitle = adviceManagement.getAdviceTitle();
            this.dateAdvice = adviceManagement.getDateAdvice();
            this.adviceContent = adviceManagement.getAdviceContent();
            this.dateRegister = adviceManagement.getDateRegister();
            this.etc = adviceManagement.getEtc();
        }

        @Override
        public AdviceManagementResponse build() {
            return new AdviceManagementResponse(this);
        }
    }
}
