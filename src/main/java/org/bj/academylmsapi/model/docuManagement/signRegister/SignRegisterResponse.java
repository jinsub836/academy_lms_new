package org.bj.academylmsapi.model.docuManagement.signRegister;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.docuManagement.SignRegister;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 서류관리 - 서류등록 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SignRegisterResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "과정 id")
    private Long subjectId;

    @Schema(description = "제목")
    private String boardTitle;

    @Schema(description = "작성자")
    private String boardWriter;

    @Schema(description = "내용")
    private String boardContent;

    @Schema(description = "첨부파일")
    private String addFile;

    @Schema(description = "등록일")
    private LocalDateTime dateCreate;

    private SignRegisterResponse(Builder builder) {
        this.id = builder.id;
        this.subjectId = builder.subjectId;
        this.boardTitle = builder.boardTitle;
        this.boardWriter = builder.boardWriter;
        this.boardContent = builder.boardContent;
        this.addFile = builder.addFile;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<SignRegisterResponse> {
        private final Long id;
        private final Long subjectId;
        private final String boardTitle;
        private final String boardWriter;
        private final String boardContent;
        private final String addFile;
        private final LocalDateTime dateCreate;

        public Builder(SignRegister signRegister) {
            this.id = signRegister.getId();
            this.subjectId = signRegister.getSubject().getId();
            this.boardTitle = signRegister.getBoardTitle();
            this.boardWriter = signRegister.getBoardWriter();
            this.boardContent = signRegister.getBoardContent();
            this.addFile = signRegister.getAddFile();
            this.dateCreate = signRegister.getDateCreate();
        }

        @Override
        public SignRegisterResponse build() {
            return new SignRegisterResponse(this);
        }
    }
}
