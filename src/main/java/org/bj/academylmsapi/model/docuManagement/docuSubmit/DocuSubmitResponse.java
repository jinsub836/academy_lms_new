package org.bj.academylmsapi.model.docuManagement.docuSubmit;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.docuManagement.DocuSubmit;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 서류관리 - 서류제출 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DocuSubmitResponse {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "서류등록 id")
    private Long signRegisterId;

    @Schema(description = "수강생 id")
    private Long studentId;

    @Schema(description = "서명파일")
    private String signFile;

    @Schema(description = "첨부파일")
    private String addFile;

    @Schema(description = "서명완료")
    private Boolean isSignClear;

    @Schema(description = "등록일")
    private LocalDateTime dateCreate;

    private DocuSubmitResponse(Builder builder) {
        this.id = builder.id;
        this.signRegisterId = builder.signRegisterId;
        this.studentId = builder.studentId;
        this.signFile = builder.signFile;
        this.addFile = builder.addFile;
        this.isSignClear = builder.isSignClear;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<DocuSubmitResponse> {
        private final Long id;
        private final Long signRegisterId;
        private final Long studentId;
        private final String signFile;
        private final String addFile;
        private final Boolean isSignClear;
        private final LocalDateTime dateCreate;

        public Builder(DocuSubmit docuSubmit) {
            this.id = docuSubmit.getId();
            this.signRegisterId = docuSubmit.getSignRegister().getId();
            this.studentId = docuSubmit.getStudent().getId();
            this.signFile = docuSubmit.getSignFile();
            this.addFile = docuSubmit.getAddFile();
            this.isSignClear = docuSubmit.getIsSignClear();
            this.dateCreate = docuSubmit.getDateCreate();
        }

        @Override
        public DocuSubmitResponse build() {
            return new DocuSubmitResponse(this);
        }
    }
}
