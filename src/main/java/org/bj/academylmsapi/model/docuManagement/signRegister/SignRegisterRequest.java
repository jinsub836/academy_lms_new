package org.bj.academylmsapi.model.docuManagement.signRegister;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

// 서류관리 - 서류등록 C model

@Getter
@Setter
public class SignRegisterRequest {
    @NotNull
    @Schema(description = "과정 id")
    private Long subjectId;

    @NotNull
    @Schema(description = "제목", minLength = 2, maxLength = 50)
    @Length(min = 2, max = 50)
    private String boardTitle;

    @NotNull
    @Schema(description = "작성자")
    private String boardWriter;

    @NotNull
    @Schema(description = "내용")
    private String boardContent;

    @NotNull
    @Schema(description = "첨부파일")
    private String addFile;
}
