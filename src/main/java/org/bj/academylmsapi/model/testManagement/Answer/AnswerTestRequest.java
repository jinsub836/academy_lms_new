package org.bj.academylmsapi.model.testManagement.Answer;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.TestStatus;

@Getter
@Setter
public class AnswerTestRequest {
    @NotNull
    @Schema(description = "시험출제 id")
    private Long makeTest;

    @NotNull
    @Schema(description = "수강생 id")
    private Long student;

    @NotNull
    @Schema(description = "답변")
    private String testAnswer;

    @Schema(description = "수강생서명")
    private String studentSign;

    @Schema(description = "교수자총평")
    private String teacherComment;

    @Schema(description = "평가점수")
    private Short studentScore;

    @Schema(description = "성취수준")
    private Short achieveLevel;

    @NotNull
    @Schema(description = "응시현황")
    private TestStatus testStatus;

    @Schema(description = "첨부파일")
    private String addFile;

}
