package org.bj.academylmsapi.model.testManagement.MakeTest;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MakeTestRequest {
    @NotNull
    @Schema(description = "과목현황 id")
    private Long subjectStatusId;

    @NotNull
    @Schema(description = "시험문제")
    private String testProblem;

    @NotNull
    @Schema(description = "객관식")
    private String testSelect;
}
