package org.bj.academylmsapi.model.testManagement.Answer;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.TestStatus;

@Setter
@Getter
public class AnswerPutStudentSignRequest {
    @Schema(description = "수강생서명")
    private String studentSign;

    @NotNull
    @Schema(description = "응시현황")
    private TestStatus testStatus;
}
