package org.bj.academylmsapi.model.testManagement.Answer;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.AnswerTest;
import org.bj.academylmsapi.enums.TestStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AnswerTestResponse {
    @Schema(description = "시험채점 id")
    private Long id;

    @Schema(description = "시험출제 과목명")
    private String makeTestName;

    @Schema(description = "수강생 이름")
    private String studentName;

    @Schema(description = "답변")
    private String[] testAnswer;

    @Schema(description = "수강생서명")
    private String studentSign;

    @Schema(description = "교수자총평")
    private String teacherComment;

    @Schema(description = "평가점수")
    private Short studentScore;

    @Schema(description = "성취수준")
    private Short achieveLevel;

    @Schema(description = "응시현황")
    private TestStatus testStatus;

    @Schema(description = "첨부파일")
    private String addFile;

    private AnswerTestResponse(Builder builder) {
        this.id = builder.id;
        this.makeTestName = builder.makeTestName;
        this.studentName = builder.studentName;
        this.testAnswer = builder.testAnswer;
        this.studentSign = builder.studentSign;
        this.teacherComment = builder.teacherComment;
        this.studentScore = builder.studentScore;
        this.achieveLevel = builder.achieveLevel;
        this.addFile = builder.addFile;
        this.testStatus = builder.testStatus;
    }

    public static class Builder implements CommonModelBuilder<AnswerTestResponse> {
        private final Long id;
        private final String makeTestName;
        private final String studentName;
        private final String[] testAnswer;
        private final String studentSign;
        private final String teacherComment;
        private final Short studentScore;
        private final Short achieveLevel;
        private final String addFile;
        private final TestStatus testStatus;

        public Builder(AnswerTest answerTest) {
            this.id = answerTest.getId();
            this.makeTestName = answerTest.getMakeTest().getSubjectStatus().getTestName();
            this.studentName = answerTest.getStudent().getStudentName();
            this.testAnswer = answerTest.getTestAnswer().split("@");
            this.studentSign = answerTest.getStudentSign();
            this.teacherComment = answerTest.getTeacherComment();
            this.studentScore = answerTest.getStudentScore();
            this.achieveLevel = answerTest.getAchieveLevel();
            this.addFile = answerTest.getAddFile();
            this.testStatus = answerTest.getTestStatus();
        }

        @Override
        public AnswerTestResponse build() {
            return new AnswerTestResponse(this);
        }
    }
}
