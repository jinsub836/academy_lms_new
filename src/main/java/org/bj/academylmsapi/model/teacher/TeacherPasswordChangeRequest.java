package org.bj.academylmsapi.model.teacher;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class TeacherPasswordChangeRequest {
    @NotNull
    @Schema(description = "비밀번호", minLength = 8)
    @Length(min = 8)
    private String password;
}
