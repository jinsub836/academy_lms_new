package org.bj.academylmsapi.model.login;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class LoginRequest {
    @NotNull
    @Schema(description = "유저 아이디", minLength = 5,maxLength = 20)

    private String username;

    @NotNull
    @Schema(description = "비밀번호", minLength = 8,maxLength = 20)

    private String password;
}
