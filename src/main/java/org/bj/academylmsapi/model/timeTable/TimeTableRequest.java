package org.bj.academylmsapi.model.timeTable;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class TimeTableRequest {
    @NotNull
    @Schema(description = "과정 id")
    private Long subjectId;

    @NotNull
    @Schema(description = "훈련일자", maxLength = 8)
    @Length(max = 8)
    private String dateTraining;

    @NotNull
    @Schema(description = "훈련시작시간", maxLength = 4)
    @Length(max = 4)
    private String startTimeTrain;

    @NotNull
    @Schema(description = "훈련종료시간", maxLength = 4)
    @Length(max = 4)
    private String endTime;

    @Schema(description = "방학/원격여부")
    private String isRemote;

    @NotNull
    @Schema(description = "시작시간", maxLength = 4)
    @Length(max = 4)
    private String startTime;

    @NotNull
    @Schema(description = "시간구분", maxLength = 1)
    @Length(max = 1)
    private String timeSorted;

    @Schema(description = "훈련강사코드", maxLength = 20)
    @Length(max = 20)
    private String teacherCode;

    @Schema(description = "교육장소(강의실)코드", maxLength = 30)
    @Length(max = 30)
    private String placeCode;

    @Schema(description = "교과목(및 능력단위) 코드", maxLength = 30)
    @Length(max = 30)
    private String subjectCode;
}
