package org.bj.academylmsapi.model.timeTable;

import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;

public class TimeTableItem {
    @Schema(description = "시간표 id")
    private Long id;

    @Schema(description = "과정 id")
    private Long subjectId;

    @Schema(description = "훈련일자")
    private String dateTraining;

    @Schema(description = "훈련시작시간")
    private String startTimeTrain;

    @Schema(description = "훈련종료시간")
    private String endTime;

    @Schema(description = "방학/원격여부")
    private String isRemote;

    @Schema(description = "시작시간")
    private String startTime;

    @Schema(description = "시간구분")
    private String timeSorted;

    @Schema(description = "훈련강사코드")
    private String teacherCode;

    @Schema(description = "교육장소(강의실)코드")
    private String placeCode;

    @Schema(description = "교과목(및 능력단위) 코드")
    private String subjectCode;
}
