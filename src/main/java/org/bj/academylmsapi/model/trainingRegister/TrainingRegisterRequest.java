<<<<<<<< HEAD:src/main/java/org/bj/academylmsapi/model/TrainingRegisterRequest.java
package org.bj.academylmsapi.model.trainingManagement;
========
package org.bj.academylmsapi.model.trainingRegister;
>>>>>>>> origin:src/main/java/org/bj/academylmsapi/model/trainingRegister/TrainingRegisterRequest.java

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 훈련관리 - 훈련관리 등록 C model

@Getter
@Setter
public class TrainingRegisterRequest {
    @Schema(description = "강좌 Id")
    private Long SubjectId;

    @Schema(description = "훈련날짜")
    private LocalDate dateTraining;

    @Schema(description = "이론")
    private Short theory;

    @Schema(description = "실습")
    private Short practice;

    @Schema(description = "훈련과목", minLength = 2, maxLength = 20)
    private String trainingSubject;

    @Schema(description = "훈련내용", minLength = 2, maxLength = 30)
    private String trainingContent;

    @Schema(description = "재적(명)")
    private Short registrationNum;

    @Schema(description = "출석(명)")
    private Short attendanceNum;

    @Schema(description = "이론(시간)", maxLength = 2)
    @Length(max = 2)
    private Short theory;

    @Schema(description = "실습(시간)", maxLength = 2)
    @Length(max = 2)
    private Short practice;

    @Schema(description = "결석(사람)")
    private String absentWho;

    @Schema(description = "지각(사람)")
    private String lateWho;

    @Schema(description = "조퇴(사람)")
    private String earlyLeaveWho;

<<<<<<<< HEAD:src/main/java/org/bj/academylmsapi/model/TrainingRegisterRequest.java
========
    @Schema(description = "훈련과목", minLength = 2, maxLength = 20)
    @Length(min = 2, max = 20)
    private String trainingSubject;

    @Schema(description = "훈련내용", minLength = 2, maxLength = 30)
    @Length(min = 2, max = 30)
    private String trainingContent;

    @Schema(description = "기타사항")
    private String etc;
>>>>>>>> origin:src/main/java/org/bj/academylmsapi/model/trainingRegister/TrainingRegisterRequest.java
}
