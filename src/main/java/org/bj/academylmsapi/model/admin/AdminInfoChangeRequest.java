package org.bj.academylmsapi.model.admin;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.Gender;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 관리자 address U model

@Getter
@Setter
public class AdminInfoChangeRequest {
    @NotNull
    @Schema(description = "관리자이름", minLength = 2, maxLength = 20)
    @Length(min = 2, max = 20)
    private String adminName;

    @NotNull
    @Schema(description = "성별", enumAsRef = true)
    private Gender gender;

    @NotNull
    @Schema(description = "아이디", minLength = 5, maxLength = 20)
    @Length(min = 5, max = 20)
    private String username;

    @NotNull
    @Schema(description = "비밀번호", minLength = 8)
    @Length(min = 8)
    private String password;

    @NotNull
    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    @NotNull
    @Schema(description = "휴대폰 번호", minLength = 13, maxLength = 13)
    @Length(min = 13, max = 13)
    private String phoneNumber;
}
