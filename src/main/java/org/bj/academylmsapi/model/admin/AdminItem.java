package org.bj.academylmsapi.model.admin;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

// 관리자 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdminItem {
    @Schema(description = "id")
    private Long id;

    @Schema(description = "관리자이름")
    private String adminName;

    @Schema(description = "성별", enumAsRef = true)
    private Gender gender;

    @Schema(description = "생년월일")
    private LocalDate dateBirth;

    private AdminItem(Builder builder) {
        this.id = builder.id;
        this.adminName = builder.adminName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
    }

    public static class Builder implements CommonModelBuilder<AdminItem> {
        private final Long id;
        private final String adminName;
        private final Gender gender;
        private final LocalDate dateBirth;

        public Builder(Admin admin) {
            this.id = admin.getId();
            this.adminName = admin.getAdminName();
            this.gender = admin.getGender();
            this.dateBirth = admin.getDateBirth();
        }

        @Override
        public AdminItem build() {
            return new AdminItem(this);
        }
    }
}
