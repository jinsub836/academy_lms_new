package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.exception.CMemberException;
import org.bj.academylmsapi.repository.AdminRepository;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final AdminRepository adminRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Admin> admin = adminRepository.findByUsername(username);
        Optional<Teacher> teacher = teacherRepository.findByUsername(username);
        Optional<Student> student = studentRepository.findByUsername(username);

        if (admin.isEmpty() && teacher.isEmpty() && student.isEmpty()) throw new CMemberException();

        if (admin.isPresent() && username.contains("admin")) return admin.get();
        else if (teacher.isPresent() && username.contains("teacher")) return teacher.get();
        else return student.get();
    }
}
