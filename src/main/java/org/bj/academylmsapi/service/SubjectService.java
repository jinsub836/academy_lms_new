package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Subject;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.subject.SubjectItem;
import org.bj.academylmsapi.model.subject.SubjectRequest;
import org.bj.academylmsapi.repository.SubjectRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


import java.util.LinkedList;
import java.util.List;

// 과정 service

@Service
@RequiredArgsConstructor
public class SubjectService {
    private final SubjectRepository subjectRepository;
    private final TeacherRepository teacherRepository;

    // 과정 C
    public void setSubjectChoice(SubjectRequest request){
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        subjectRepository.save(new Subject.Builder(request,teacherData).build());
    }

    // 과정 복수 R
    public List<SubjectItem> getSubjectList(){
        List<Subject> originData = subjectRepository.findAll();
        List<SubjectItem> result = new LinkedList<>();
        for (Subject subject : originData) result.add(new SubjectItem.Builder(subject).build());
        return result;
    }

    // 과정 복수 R 페이징
    public ListResult<SubjectItem> getSubjectPageList(int pageNum){
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Subject> subjectChoicePage = subjectRepository.findAll(pageRequest);
        List<SubjectItem> result = new LinkedList<>();
        for (Subject subject : subjectChoicePage) result.add(new SubjectItem.Builder(subject).build());
        return ListConvertService.settingListResult(result, subjectChoicePage.getTotalElements(), subjectChoicePage.getTotalPages(), subjectChoicePage.getPageable().getPageNumber());
    }


    // 과정 U
    public void putSubjectChoice(long id , SubjectRequest request){
        Subject subject = subjectRepository.findById(id).orElseThrow();
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();

        subject.putSubject(teacherData, request);
        subjectRepository.save(subject);
    }

    // 과정 D
    public void delSubject(long id){
        subjectRepository.deleteById(id);
    }
}
