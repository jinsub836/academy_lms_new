package org.bj.academylmsapi.service.studentManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentManagement.DesireWork;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.desireWork.DesireWorkRequest;
import org.bj.academylmsapi.model.studentManagement.desireWork.DesireWorkResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentManagement.DesireWorkRepository;
import org.springframework.stereotype.Service;

// 학생관리 - 취업관리 - 희망근무조건 service

@Service
@RequiredArgsConstructor
public class DesireWorkService {
    private final DesireWorkRepository desireWorkRepository;
    private final StudentRepository studentRepository;

    // 희망근무조건 C
    public void setDesireWork(DesireWorkRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        desireWorkRepository.save(new DesireWork.Builder(request, studentData).build());
    }

    // 희망근무조건 단수 R
    public DesireWorkResponse getDesireWork(long id) {
        DesireWork originData = desireWorkRepository.findById(id).orElseThrow();
        return new DesireWorkResponse.Builder(originData).build();
    }

    // 희망근무조건 U
    public void putDesireWork(long id, DesireWorkRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        DesireWork originData = desireWorkRepository.findById(id).orElseThrow();
        originData.putDesireWork(request, studentData);
        desireWorkRepository.save(originData);
    }

    // 희망근무조건 D
    public void delDesireWork(long id) {
        desireWorkRepository.deleteById(id);
    }
}
