package org.bj.academylmsapi.service.docuManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.docuManagement.DocuSubmit;
import org.bj.academylmsapi.entity.docuManagement.SignRegister;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.docuManagement.docuSubmit.DocuSubmitRequest;
import org.bj.academylmsapi.model.docuManagement.docuSubmit.DocuSubmitResponse;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.docuManagement.DocuSubmitRepository;
import org.bj.academylmsapi.repository.docuManagement.SignRegisterRepository;
import org.springframework.stereotype.Service;

// 서류관리 - 서류제출 service

@Service
@RequiredArgsConstructor
public class DocuSubmitService {
    private final DocuSubmitRepository docuSubmitRepository;
    private final SignRegisterRepository signRegisterRepository;
    private final StudentRepository studentRepository;

    // 서류제출 C
    public void setDocuSubmit(DocuSubmitRequest request) {
        SignRegister signRegisterData = signRegisterRepository.findById(request.getSignRegisterId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        docuSubmitRepository.save(new DocuSubmit.Builder(request,signRegisterData, studentData).build());
    }

    // 서류제출 단수 R
    public DocuSubmitResponse getDocuSubmit(long id) {
        DocuSubmit originData = docuSubmitRepository.findById(id).orElseThrow();
        return new DocuSubmitResponse.Builder(originData).build();
    }

    // 서류제출 U
    public void putDocuSubmit(long id, DocuSubmitRequest request) {
        DocuSubmit originData = docuSubmitRepository.findById(id).orElseThrow();
        SignRegister signRegisterData = signRegisterRepository.findById(request.getSignRegisterId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        originData.putDocuSubmit(request, signRegisterData, studentData);
        docuSubmitRepository.save(originData);
    }

    // 서류제출 D
    public void delDocuSubmit(long id) {
        docuSubmitRepository.deleteById(id);
    }
}
