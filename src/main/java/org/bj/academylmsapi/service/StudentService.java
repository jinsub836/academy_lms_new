package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.exception.CMemberException;
import org.bj.academylmsapi.lib.CommonCheck;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.student.*;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.SubjectChoiceRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 수강생 service

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final SubjectChoiceRepository subjectChoiceRepository;
    private final ProfileService profileService;
    private final PasswordEncoder passwordEncoder;

    // id 찾아오기
    public Student getData(long id) {
        return studentRepository.findById(id).orElseThrow();
    }

    // 수강생 C
    public void setStudent(StudentRequest request) {
        SubjectChoice subjectChoiceData = subjectChoiceRepository.findById(request.getSubjectChoiceId()).orElseThrow();
        if (!CommonCheck.checkUsername(request.getUsername())) throw new CMemberException(); // 아이디 형식 검사
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberException(); // 비밀번호 재검
        if (!usernameDupCheck(request.getUsername())) throw new CMemberException(); // 아이디 중복 체크

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Student student = new Student.Builder(request, subjectChoiceData).build();

        studentRepository.save(student);
    }

    // 수강생 복수 R
    public List<StudentItem> getStudents() {
        List<Student> originData = studentRepository.findAllByOrderByIdAsc();
        List<StudentItem> result = new LinkedList<>();
        for (Student student : originData) result.add(new StudentItem.Builder(student).build());
        return result;
    }

    // 수강생 복수 R 페이징
    public ListResult<StudentItem> getStudentsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Student> studentPage = studentRepository.findAll(pageRequest);
        List<StudentItem> result = new LinkedList<>();
        for (Student student : studentPage) result.add(new StudentItem.Builder(student).build());
        return ListConvertService.settingListResult(result, studentPage.getTotalElements(), studentPage.getTotalPages(), studentPage.getPageable().getPageNumber());
    }

    // 수강생 단수 R ToKen 사용 하여서 보는법
    public StudentResponse getStudent() {
        Student originData = profileService.getStudentData();
        return new StudentResponse.Builder(originData).build();
    }

    // 수강생 단수 R
    public StudentResponse getStudentDetail(Long id) {
        Student originData = studentRepository.findById(id).orElseThrow();
        return new StudentResponse.Builder(originData).build();
    }



    // 수강생 U
    public void putStudent( StudentInfoChangeRequest studentInfoChangeRequest) {
        Student originData = profileService.getStudentData();
        originData.putStudentInfo(studentInfoChangeRequest);
        studentRepository.save(originData);
    }

    // 수강생 U 관리자용
    public void putStudentAdmin(StudentInfoChangeAdminRequest studentInfoChangeAdminRequest) {
        Student originData = profileService.getStudentData();
        SubjectChoice subjectChoiceData = subjectChoiceRepository.findById(studentInfoChangeAdminRequest.getSubjectChoiceId()).orElseThrow();
        originData.putStudentInfoAdmin(subjectChoiceData, studentInfoChangeAdminRequest);
        studentRepository.save(originData);

    }

    // 수강생 D
    public void delStudent() {
        studentRepository.deleteById(profileService.getStudentData().getId());
    }

    private boolean usernameDupCheck(String username){
        long dupCount = studentRepository.countByUsername(username);
        return dupCount <= 0;
    }
}
