package org.bj.academylmsapi.service.studentAttendance;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalFixRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalItem;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceApprovalRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ProfileService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;


// 수강생 출결 승인 service

@Service
@RequiredArgsConstructor
public class StudentAttendanceApprovalService {
    private final StudentAttendanceApprovalRepository studentAttendanceApprovalRepository;
    private final StudentAttendanceService studentAttendanceService;
    private final StudentRepository studentRepository;
    private final ProfileService profileService;

    /**
     * 수강생 출결 승인 C
     */
    public void setStudentAttendanceApproval(StudentAttendanceApprovalRequest request) throws ParseException {
        Student studentData = profileService.getStudentData();
        studentAttendanceApprovalRepository.save(new StudentAttendanceApproval.Builder(request, studentData).build());
    }

    public void setStudentAttendanceApprovalById(Long id) {
        Student studentData = studentRepository.findAllById(id).orElseThrow();
        StudentAttendanceApproval attendanceApproval = new StudentAttendanceApproval();
        attendanceApproval.setStudent(studentData);
        attendanceApproval.setReason("금일 결석");
        attendanceApproval.setDateStart(LocalDate.now());
        attendanceApproval.setDateEnd(LocalDate.now());
        attendanceApproval.setAttendanceType(AttendanceType.ABSENT);
        attendanceApproval.setDateRegister(LocalDate.now());
        attendanceApproval.setApprovalState(ApprovalState.NOTAPPROVED);
        studentAttendanceApprovalRepository.save(attendanceApproval);
    }

    public void setStudentLateApprovalById(Long id) {
        Student studentData = studentRepository.findAllById(id).orElseThrow();
        StudentAttendanceApproval attendanceApproval = new StudentAttendanceApproval();
        attendanceApproval.setStudent(studentData);
        attendanceApproval.setReason("금일 지각");
        attendanceApproval.setDateStart(LocalDate.now());
        attendanceApproval.setDateEnd(LocalDate.now());
        attendanceApproval.setAttendanceType(AttendanceType.LATE);
        attendanceApproval.setDateRegister(LocalDate.now());
        attendanceApproval.setApprovalState(ApprovalState.NOTAPPROVED);
        studentAttendanceApprovalRepository.save(attendanceApproval);
    }

    public void setStudentEarlyApprovalById(Long id) {
        Student studentData = studentRepository.findAllById(id).orElseThrow();
        StudentAttendanceApproval attendanceApproval = new StudentAttendanceApproval();
        attendanceApproval.setStudent(studentData);
        attendanceApproval.setReason("금일 조퇴");
        attendanceApproval.setDateStart(LocalDate.now());
        attendanceApproval.setDateEnd(LocalDate.now());
        attendanceApproval.setAttendanceType(AttendanceType.EARLYLEAVE);
        attendanceApproval.setDateRegister(LocalDate.now());
        attendanceApproval.setApprovalState(ApprovalState.NOTAPPROVED);
        studentAttendanceApprovalRepository.save(attendanceApproval);
    }

    // 수강생 출결 승인 복수 R
    public List<StudentAttendanceApprovalItem> getStudentAttendanceApprovals() {
        List<StudentAttendanceApproval> originList = studentAttendanceApprovalRepository.findAll();
        List<StudentAttendanceApprovalItem> result = new LinkedList<>();
        for (StudentAttendanceApproval studentAttendanceApproval : originList)
            result.add(new StudentAttendanceApprovalItem.Builder(studentAttendanceApproval).build());
        return result;
    }

    // 수강생 출결 승인 복수 R 페이징
    public ListResult<StudentAttendanceApprovalItem> getStudentAttendanceApprovalsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<StudentAttendanceApproval> studentAttendanceApprovalPage = studentAttendanceApprovalRepository.findAll(pageRequest);
        List<StudentAttendanceApprovalItem> result = new LinkedList<>();
        for (StudentAttendanceApproval studentAttendanceApproval : studentAttendanceApprovalPage)
            result.add(new StudentAttendanceApprovalItem.Builder(studentAttendanceApproval).build());
        return ListConvertService.settingListResult(result);
    }

    // 수강생 출결수정 U
    public void putStudentAttendance(long id, StudentAttendanceApprovalFixRequest request) {
        StudentAttendanceApproval originData = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceApproval(request);
        studentAttendanceApprovalRepository.save(originData);
    }

    //수강생 출결 승인
    public void putStudentAttendanceApprovalAdmin(long id){
        StudentAttendanceApproval originData = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceApprovalAdmin();
        studentAttendanceApprovalRepository.save(originData);

        studentAttendanceService.putAdminApproval(originData.getStudent().getId(),originData.getAttendanceType());
    }


    // 수강생 출결승인 D
    public void delStudentAttendanceApproval(long id) {
        studentAttendanceApprovalRepository.deleteById(id);
    }
}
