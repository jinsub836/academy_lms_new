package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.timeTable.TimeTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeTableRepository extends JpaRepository<TimeTable, Long> {
}
