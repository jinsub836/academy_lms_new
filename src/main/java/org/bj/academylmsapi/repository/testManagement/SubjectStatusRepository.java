package org.bj.academylmsapi.repository.testManagement;

import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.springframework.data.jpa.repository.JpaRepository;

// 평가관리 - 과목현황

public interface SubjectStatusRepository extends JpaRepository<SubjectStatus, Long> {
}
