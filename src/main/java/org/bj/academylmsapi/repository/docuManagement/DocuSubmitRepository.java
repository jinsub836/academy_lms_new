package org.bj.academylmsapi.repository.docuManagement;

import org.bj.academylmsapi.entity.docuManagement.DocuSubmit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocuSubmitRepository extends JpaRepository<DocuSubmit, Long> {
}
