package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

// 수강생 repository

public interface StudentRepository extends JpaRepository<Student, Long> {

    // 수강생 찾기
    Student findByStudentName(String name);

    // 수강생 복수 R 등록순 정렬
    List<Student> findAllByOrderByIdAsc();

    Optional<Student> findAllById(Long id);

    Optional<Student> findByUsername(String username);

    List<Student> findBySubjectChoiceId(Long subjectId);

    //중복 체크

    long countByUsername(String username);
}
