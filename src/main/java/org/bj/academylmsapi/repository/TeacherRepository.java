package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

// 강사 repository

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    Optional<Teacher> findAllById(Long id);

    Optional<Teacher> findByUsername(String username);

    //중복 체크

    long countByUsername(String username);

}
