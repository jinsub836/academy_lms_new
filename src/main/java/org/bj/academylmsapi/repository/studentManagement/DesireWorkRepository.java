package org.bj.academylmsapi.repository.studentManagement;

import org.bj.academylmsapi.entity.studentManagement.DesireWork;
import org.springframework.data.jpa.repository.JpaRepository;

// 학생관리 - 취업관리 - 희망 근무조건

public interface DesireWorkRepository extends JpaRepository<DesireWork, Long> {
}
