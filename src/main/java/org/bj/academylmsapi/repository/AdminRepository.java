package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

// 관리자 repository

public interface AdminRepository extends JpaRepository<Admin, Long> {
    // 관리자 복수 R 등록순 정렬
    List<Admin> findAllByOrderByIdAsc();

    //있을 수도 있고 없을 수도 있는 값을 찾아야 함으로 Optional 만약 Optional이 없으면 repository에서
    //던저지므로 메세지를 전달할 수 없음
    Optional<Admin> findByUsername(String username);

    //중복 체크

    long countByUsername(String username);

}
