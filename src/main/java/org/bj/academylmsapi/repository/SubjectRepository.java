package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

// 과정 repository

public interface SubjectRepository extends JpaRepository<Subject, Long> {
}
