package org.bj.academylmsapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    //초기 설정에 들어가야 하기 때문에 Bean 어노테이션 달아준다.
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        // 오리진 뭔지
        config.setAllowedOriginPatterns(List.of("*"));
        // 매핑 이름들인데 제일 중요한건 옵션즈이다.freeFlight(프리플라이트)
        config.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        // 모든 요청 다 풀어준다.
        config.setAllowedHeaders(List.of("*"));
        // 노출된 헤더 다 허용
        config.setExposedHeaders(List.of("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // 모든 매핑 주소에 대해서 위에 설정을 먹이겠다.
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Bean
    // 시큐리티 설정을 체인으로 표현한다.
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // csrf를 비활성화 왜 해야하는지 알아보자
        http.csrf(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource())) // 이제 시큐리티한테 부탁해야하기 때문에 체인으로 묶는다.
                .sessionManagement((sessionManagement) ->
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) // 위에 csrf 때문에 스테이스리스 이다.
                .authorizeHttpRequests((authorizeRequests) -> //인증 맞는지 확인받는거 행위 / 인가 인증요청 온걸 허가하는행위 // 요청에 대해서 허가 인가를 해준다.
                        authorizeRequests
                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll()
                                .requestMatchers(HttpMethod.GET, "/exception/**").permitAll() // 누구나 봐야하는 메세지 먼저 허가해준다.
                                .requestMatchers("/v1/**").permitAll()
                                .requestMatchers("/**").permitAll()
                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN") // 권한 설정
                                .anyRequest().hasRole("ADMIN") // 그 외 설정
                );
        // 입셉션 메세지 처리하는 핸들러
        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied));
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));
        // 이거 수행하기 전에 맨처음에 토큰프로바이더한테 먼저 필터에 자리에서
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
        // 작업대 위에서 작업을 하니 별도에 담아서 넘겨줘야 하지 않아도 된다.
        // 빌드 리턴한다.
        return http.build();
    }
}
