package org.bj.academylmsapi;

import org.apache.logging.log4j.util.PropertySource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SpringBootTest
class AcademyLmsApiApplicationTests {

	@Test
	void streamTest() {
		String[] names = {"박진수","김진수","김하림","박하림", "임인영","김인영","김봉주","김승희"};

		String[] stream = Arrays.stream(names)
				.filter(h -> h.startsWith("김"))
				.sorted()
				.toArray(String[]::new);



		System.out.println("-----------------------------------");
		System.out.println(stream);
		System.out.println("-----------------------------------");
    }

	@Test
	void convertStringToTime(){
		String formatedNow = "0910";
		String split = ":";
		String oneTwo = formatedNow.substring(0,2);
		String threeFour = formatedNow.substring(2,4);
		System.out.println("-----------------------------------");
		System.out.println(formatedNow);
	 	System.out.println(oneTwo+":"+threeFour);
		System.out.println("-----------------------------------");

	}
	@Test
	void convertStringToData() throws ParseException {
		String testDate = "0910";

		SimpleDateFormat formatter1 = new SimpleDateFormat("HHmm");
		SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm");

		Date formatDate = formatter1.parse(testDate);

		String strNewFormatDate =  formatter2.format(formatDate);
		System.out.println("-----------------------------------");
		System.out.println(formatDate);
		System.out.println(strNewFormatDate);
		System.out.println("-----------------------------------");

	}

	@Test
	void test() {
		int[] texts = {2010, 2934, 1923};
		Integer[] test = Arrays.stream(texts).boxed().sorted(Comparator.reverseOrder()).toArray(Integer[]::new);
		System.out.println("---------------------------------------------");
		System.out.println(Arrays.toString(test));
		System.out.println("---------------------------------------------");
	}

	@Test
	void test2() {
		String[] names = {"홍길동", "박기기", "박길동", "김길동", "김기기", "김남호", "킹남호", "김명돈", "킹초코", "김초코"};
		System.out.println("---------------------------------------------");
		Arrays.stream(names).filter(a -> a.startsWith("김")).forEach(System.out::println);
		System.out.println("---------------------------------------------");
	}

	@Test
	void test3() {
		int test = 4/3;
		System.out.println("---------------------------------------------");
		System.out.println(test);
		System.out.println("---------------------------------------------");
	}

}
